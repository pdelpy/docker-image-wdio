# docker-image-wdio

Purpose of this repository : It is a simple `docker images` to have a system dépendancies for running a `webdriverio` project

## What is Webdriverio

Webdriverio is a end to end test automation framework. It is a written with (`javascript` / `typescript`) and use nodeJs framework.
We use also BDD process with Cucumber framework and use Alure report.We also add CucumberStudio support with adding `hiptest-publicher` library

