FROM openjdk:15-alpine as builder

FROM node:13-alpine3.11

# default user in nodejs container
ENV USER_APP=node
ENV GLIB_VERSION=2.29-r0
ENV JAVA_HOME=/opt/jdk
ENV PATH=$JAVA_HOME/bin:$PATH

# ADD JAVA-15 ON NODEJS ALPINE 
COPY --from=builder /opt/openjdk-15/ /opt/jdk/

# ADD SOMME LIB TO BUILD FIBER (NODEJS LIB) and ruby for CUCUMBERSTUDIO SUPPORT 
RUN apk add --no-cache \
    python \
    g++ \
    curl \
    make \
    ruby-dev \
    ruby \
    ruby-rdoc \
    libpng-dev

# BROWSERSTACK LIB TO ADD FOR ALPINE 
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIB_VERSION}/glibc-${GLIB_VERSION}.apk && \
    apk add glibc-${GLIB_VERSION}.apk && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIB_VERSION}/glibc-bin-${GLIB_VERSION}.apk && \
    apk add glibc-bin-${GLIB_VERSION}.apk && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIB_VERSION}/glibc-i18n-${GLIB_VERSION}.apk && \
    apk add glibc-i18n-${GLIB_VERSION}.apk && \
    rm -rf glibc-*.apk && \
    rm -rf /root/.cache

# SET WDIO TO CURRENT USER
USER ${USER_APP}

# SET WDIO HOME DIR TO THE WORKDIR
WORKDIR /home/${USER_APP}/
